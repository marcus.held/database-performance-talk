# Structure

## Preparation

- Open IDE in presentation mode and make sure that the Popup is working
- Start database
- Execute `SELECT pg_stat_statements_reset();`

## Debugging

- checkout `debugging`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration scan="true" scanPeriod="5 seconds">
    <include resource="org/springframework/boot/logging/logback/base.xml"/>

    <logger name="org.hibernate.SQL" level="DEBUG"/>
    <logger name="org.hibernate.type.descriptor.sql.BasicBinder" level="TRACE"/>
</configuration>

```

## Understanding Hibernate Statements

- checkout `hibernate`
- Explain setup
- Show log output for `create`

Problems:
### No Transaction
Show error with exception in database
```kotlin
@ShellMethod("create a complex entity")
fun createComplex() {
    val composite1 = compositeRepository.save(Composite(1))
    val composite2 = compositeRepository.save(Composite(2))
    if (1 == 1) throw IllegalStateException("Oups")
    complexRepository.save(Complex(listOf(composite1, composite2)))
}
```

- Add logging

```xml
    <logger name="org.springframework.transaction.interceptor" level="TRACE"/>
```

- Add `@Transactional`
- Show that no insert is done
- Remove Exception
- Show transaction handling

### Too many save statements

Composites should only exist with a complex type
```kotlin
@ShellMethod("create a complex entity")
@Transactional
fun createComplex() {
    complexRepository.save(Complex(listOf(Composite(1), Composite(2))))
}
```

- Show error (Entity not found) - We don't cascade the PERSIST operation

```kotlin
@OneToMany(cascade = [CascadeType.PERSIST]) var compositeList: List<Composite>
```

### Unnecessary associative table

- Show database diagram
- We don't need the associative table

```kotlin
@JoinColumn
```

### Unnecessary updates

- The Hibernate flush order executes persist operation before collection elements are handled
- Is it a problem? Probably not
- If you identify this as a performance problem you can model it as a bidirectional association

```kotlin
@Entity
class Composite(
    var attribute: Int,
) : BaseEntity() {
    @ManyToOne lateinit var parent: Complex
}

@Entity
class Complex(
    @OneToMany(cascade = [CascadeType.PERSIST], mappedBy = "parent")
    var compositeList: List<Composite>
) : BaseEntity()
```

- Show log output
- Linking didn't work

```kotlin
        val complex = Complex(mutableListOf())
        val composite1 = Composite(1, complex)
        val composite2 = Composite(2, complex)
        complex.compositeList.add(composite1)
        complex.compositeList.add(composite2)
```

- Only introduce this complexity in your model if it really is a performance problem.

## Hibernate vs SQL

- switch to hibernateVsSQL
- explain setup
- we can improve drastically by using SQL

```kotlin
@Repository
interface AppUserRepository : BaseJpaRepository<AppUser> {

    @Query(nativeQuery = true, value = """
SELECT COUNT(DISTINCT w.*)
    FROM (SELECT DISTINCT a.threshold_in_watt, sum(dc.consumption) OVER (PARTITION BY d.devices_id) AS sum
            FROM app_user a
            JOIN device d on a.id = d.devices_id
            JOIN device_consumption dc on d.id = dc.device_id
         ) as w
    WHERE w.sum > w.threshold_in_watt
    """)
    fun countAllByExceededThreshold(): Int
}
```

```kotlin

@ShellMethod("Counts all users which have devices with exceeded threshold")
fun sqlCount() {
    val measuredTime = measureTimeMillis {
        transactionTemplate.execute {
            println("${userRepository.countAllByExceededThreshold()} of ${userRepository.count()} users have exceeded the threshold")
        }
    }
    println("Operation took $measuredTime ms")
}
```

- Show difference with 1000 users

## Indices

```sql
SELECT pg_stat_statements_reset();
```

- Checkout `indices`
- Show slow query
- Show pg_stat_statements view
- show explain

```kotlin
@Table(indexes = [
    Index(name = "idx_device_name", columnList = "name")
])
```

-show improvement
