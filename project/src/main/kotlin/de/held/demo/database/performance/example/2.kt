package de.held.demo.database.performance.example

/*
@Entity
class Composite(
    var attribute: Int
) : BaseEntity()

@Entity
class Complex(
    @OneToMany var compositeList: List<Composite>
) : BaseEntity()

@Repository
interface ComplexRepository : BaseJpaRepository<Complex>

@Repository
interface CompositeRepository : BaseJpaRepository<Composite>

@ShellComponent
class ComplexController(
    private val complexRepository: ComplexRepository,
    private val compositeRepository: CompositeRepository
) {

    @ShellMethod("create a complex entity")
    fun createComplex() {
        val composite1 = compositeRepository.save(Composite(1))
        val composite2 = compositeRepository.save(Composite(2))
        complexRepository.save(Complex(listOf(composite1, composite2)))
    }

}
*/
