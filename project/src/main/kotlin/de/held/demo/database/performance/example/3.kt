package de.held.demo.database.performance.example
/*
import org.springframework.context.annotation.Configuration
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.stereotype.Repository
import org.springframework.transaction.support.TransactionTemplate
import javax.annotation.PostConstruct
import javax.persistence.CascadeType
import javax.persistence.ElementCollection
import javax.persistence.Embeddable
import javax.persistence.Entity
import javax.persistence.OneToMany
import kotlin.random.Random
import kotlin.system.measureTimeMillis

@Entity
class AppUser(
    @OneToMany(cascade = [CascadeType.ALL]) var devices: List<Device> = listOf(),
    var thresholdInWatt: Int
) : BaseEntity() {

    fun isThresholdExceeded() = devices.any { it.getTotalConsumption() > thresholdInWatt }

}

@Entity
class Device(
    @ElementCollection var consumption: List<EnergyConsumption> = listOf()
) : BaseEntity() {
    fun getTotalConsumption() = consumption.map { it.consumption }.sum()
}

@Embeddable
class EnergyConsumption(
    var consumption: Int
)

@Repository
interface AppUserRepository : BaseJpaRepository<AppUser>

@Configuration
class CreateDummyData(
    private val userRepository: AppUserRepository
) {

    @PostConstruct
    fun createDummyData() {
        for (i in 1..100) {
            val appUser = AppUser(createDummyDeviceList(), random(20, 100))
            userRepository.save(appUser)
        }
    }

    private fun random(from: Int, until: Int) = Random.Default.nextInt(from, until)
    private fun random(until: Int) = random(0, until)

    private fun createDummyDeviceList() = (1..(random(5)))
        .map { createDummyDevice() }

    private fun createDummyDevice() = Device(createDummyEnergyConsumption())

    private fun createDummyEnergyConsumption() = (1..(random(100)))
        .map { EnergyConsumption(random(1, 3)) }
}

@ShellComponent
class DeviceController(
    private var userRepository: AppUserRepository,
    private var transactionTemplate: TransactionTemplate
) {

    @ShellMethod("Counts all users which have devices with exceeded threshold")
    fun count() {
        val measuredTime = measureTimeMillis {
            transactionTemplate.execute {
                val allUsers = userRepository.findAll()
                val countOfUsersWithExceededThreshold = allUsers
                    .filter { it.isThresholdExceeded() }
                    .size
                println("$countOfUsersWithExceededThreshold of ${allUsers.size} users have exceeded the threshold")
            }
        }
        println("Operation took $measuredTime ms")
    }

}
*/
