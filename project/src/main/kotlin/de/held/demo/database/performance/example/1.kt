package de.held.demo.database.performance.example

/*
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.stereotype.Repository
import javax.persistence.Entity

@Entity
class Simple(
    var name: String
) : BaseEntity()

@Repository
interface SimpleRepository : BaseJpaRepository<Simple>

@ShellComponent
class Controller(val repository: SimpleRepository) {

    @ShellMethod("Create a new entity")
    fun create() {
        repository.save(Simple("foo"))
    }
}
*/
