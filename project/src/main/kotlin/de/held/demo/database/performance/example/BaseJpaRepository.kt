package de.held.demo.database.performance.example

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean
import java.util.UUID


/**
 * Interface that makes it easier to work with BaseEntity. Taken from spring-blueprint
 * (https://github.com/grandcentrix/grandcentrix-spring-blueprint/blob/master/src/main/kotlin/net/grandcentrix/spring/system/database/BaseJpaRepository.kt)
 */
@NoRepositoryBean
interface BaseJpaRepository<T : BaseEntity> : JpaRepository<T, UUID>
