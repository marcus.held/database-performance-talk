package de.held.demo.database.performance.example

import java.util.UUID
import javax.persistence.Id
import javax.persistence.MappedSuperclass
import javax.persistence.Version

/**
 * BaseEntity from spring-blueprint.
 * (https://github.com/grandcentrix/grandcentrix-spring-blueprint/blob/master/src/main/kotlin/net/grandcentrix/spring/system/database/BaseEntity.kt)
 *
 * Implements best practices from architecture board: https://stackoverflow.com/c/grandcentrix/questions/218
 */
@MappedSuperclass
abstract class BaseEntity(
    @Id var id: UUID = UUID.randomUUID()
) {

    @Version
    var version: Long? = null

    override fun equals(other: Any?): Boolean {
        return when {
            this === other -> true
            javaClass != other?.javaClass -> false
            (other is BaseEntity) && (id != other.id) -> false
            else -> true
        }
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
