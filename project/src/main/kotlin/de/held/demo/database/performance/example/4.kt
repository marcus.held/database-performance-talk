package de.held.demo.database.performance.example

import org.springframework.context.annotation.Configuration
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.stereotype.Repository
import org.springframework.transaction.support.TransactionTemplate
import java.util.UUID
import javax.annotation.PostConstruct
import javax.persistence.Entity
import kotlin.system.measureTimeMillis

@Entity
class Device(
    var name: String
) : BaseEntity()

@Repository
interface DeviceRepository : BaseJpaRepository<Device> {
    fun findByName(name: String): Device
}

@Configuration
class CreateDummyData(
    private val deviceRepository: DeviceRepository
) {

    @PostConstruct
    fun createDummyData() {
        (0..100_000)
            .map { Device(UUID.randomUUID().toString()) }
            .let(deviceRepository::saveAll)
        deviceRepository.save(Device("SecretDevice"))
    }

}

@ShellComponent
class DeviceController(
    private val deviceRepository: DeviceRepository,
    private var transactionTemplate: TransactionTemplate
) {

    @ShellMethod("Counts all users which have devices with exceeded threshold")
    fun find() {
        val measuredTime = measureTimeMillis {
            transactionTemplate.execute {
                val userWithSecretDevice = deviceRepository.findByName("SecretDevice")
                println("The user with the secret device has the id ${userWithSecretDevice.id}")
            }
        }
        println("Operation took $measuredTime ms")
    }

}

